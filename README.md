# Deep Search

Library implements deep searching in arrays by keywords. Finds elements which may include infinite nested objects.
Takes three parameters: keyword, array and one optional parameter: "excluded" object that contains two properties: "props" - array of keys you want to exclude from searching and "isInAllNesting" - boolean that defines if exclude your "props" from all nesting or only from the first level of nesting and returns a filtered array.

## Installation

```js
npm i @dionya_z/deep-search
```

## Usage example

You can see a React.js usage example, but you can also use it with React Native, Vue.js, Angular.js and other JavaScript entities or frameworks.

```js
import React, { useState } from 'react';
import deepSearch from '@dionya_z/deep-search';

const boxes = [
  {
    id: 1,
    width: 150,
    height: 150,
    color: 'Aqua',
  },
  {
    id: 2,
    width: 150,
    height: 100,
    color: 'Yellow',
  },
  {
    id: 3,
    width: 180,
    height: 200,
    color: 'AntiqueWhite',
    // Additional nested object:
    nestedBox: {
      anotherNestedBox: {
        width: 300,
        height: 200,
        content: [
          'Flashlight',
          'Toy',
          {
            anotherNestedBox: {
              isNested: true,
              id: 1,
              anythingElse: [
                'Hello',
                {
                  o: 'World',
                  weirdNesting: {
                    someProp: 'Love letter',
                  },
                },
                999,
              ],
            },
          },
        ],
      },
    },
  },
];

export const BoxesComponent = () => {
  const [filteredBox, setFilteredBox] = useState(boxes);
  const [searchInputValue, setSearchInputValue] = useState('');

  const findBoxes = e => {
    setSearchInputValue(e.target.value);

    setFilteredBox(
      deepSearch(
        e.target.value,
        boxes,
        // Optional object to exclude some
        // props from the searching
        {
          props: ['id', 'width'],
          isInAllNesting: true,
        },
      ),
    );
  };

  return (
    <div>
      <label htmlFor="input">Find boxes by infinite nesting</label>

      <input id="input" onChange={findBoxes} value={searchInputValue} />

      <div>
        {filteredBox.map(box => (
          <div
            key={box.id}
            style={{
              background: box.color,
              height: box.height,
              width: box.width,
            }}
          >
            <div>Width: {box.width}</div>

            <div>Height: {box.height}</div>

            <div>Color: {box.color}</div>
          </div>
        ))}
      </div>
    </div>
  );
};
```
